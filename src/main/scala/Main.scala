import categorytheory.{Bifunctor, Cons, Functor, List, Nil, Option}

object Main extends App {

  implicit val listFunctor: Functor[List] = categorytheory.ListFunctor
  implicit val listBifunctor: Bifunctor[List] = categorytheory.ListBifunctor
  implicit val optionFunctor: Functor[Option] = categorytheory.OptionFunctor
  import categorytheory.Functor.FunctorOps
  import categorytheory.Bifunctor.BifunctorOps

  val list: List[Int] = Cons(1, categorytheory.Cons(2, Nil))

  println(
    list.map(_ * 5)
  )

  println(
    Some(1).map(_ + " is a number")
  )

  println(
    list
      .tupleize(_.toString, _ % 2 == 0)
      .bimap(_.length, _.toString + " as a string")
  )

}
