package categorytheory

trait Bifunctor[F[_]] {
  def bimap[FROM1, FROM2, TO1, TO2](instance: F[(FROM1,FROM2)], f1: FROM1 => TO1, f2: FROM2 => TO2): F[(TO1, TO2)]
}


object Bifunctor {

  implicit class BifunctorOps[FROM1, FROM2, F[_]](instance: F[(FROM1, FROM2)]) {

    def bimap[TO1, TO2](mapping1: FROM1 => TO1, mapping2: FROM2 => TO2)(implicit f: Bifunctor[F]): F[(TO1, TO2)] = {
      f.bimap(instance, mapping1, mapping2)
    }

  }

}
