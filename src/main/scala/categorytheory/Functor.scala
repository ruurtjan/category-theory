package categorytheory

trait Functor[F[_]] {
  def map[FROM, TO](instance: F[FROM], f: FROM => TO): F[TO]
}


object Functor {

  implicit class FunctorOps[FROM, F[_]](instance: F[FROM]) {

    def map[TO](mapping: FROM => TO)(implicit f: Functor[F]): F[TO] = {
      f.map(instance, mapping)
    }

    def tupleize[TO1, TO2](f1: FROM => TO1, f2: FROM => TO2)(implicit f: Functor[F]): F[(TO1, TO2)] = {
      f.map(instance, (element: FROM) =>
        (f1(element), f2(element))
      )
    }

  }

}
