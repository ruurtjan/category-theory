package categorytheory


sealed trait List[+A]

object Nil extends List[Nothing]

case class Cons[A](element: A, next: List[A]) extends List[A]

object List {

  implicit class ListOperations[+A](instance: List[A]) {

    /**
      * `safeHead` is a natural transformation between List and Option
      *
      * @return
      */
    def safeHead(): Option[A] = instance match {
      case Nil => None
      case Cons(e, _) => Some(e)
    }
  }

}

object ListFunctor extends Functor[List] {
  override def map[B, C](list: List[B], f: B => C): List[C] = list match {
    case Nil => Nil
    case Cons(element, next) => Cons(f(element), map(next, f))
  }
}

object ListBifunctor extends Bifunctor[List] {
  override def bimap[FROM1, FROM2, TO1, TO2](list: List[(FROM1, FROM2)], f1: FROM1 => TO1, f2: FROM2 => TO2): List[(TO1, TO2)] = list match {
    case Nil => Nil
    case Cons((left: FROM1, right: FROM2), next) =>
      Cons((f1(left), f2(right)), bimap(next, f1, f2))
  }
}
