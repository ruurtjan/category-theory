package categorytheory


sealed trait Option[+A]

object None extends Option[Nothing]

case class Some[A](value: A) extends Option[A]


object OptionFunctor extends Functor[Option] {
  override def map[B, C](option: Option[B], f: B => C): Option[C] = option match {
    case None => None
    case Some(value) => Some(f(value))
  }
}
